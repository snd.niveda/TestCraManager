FROM tomcat
MAINTAINER Smartecs

COPY ./target/CRAManagerTest-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run", "-Dport.http=8888"]