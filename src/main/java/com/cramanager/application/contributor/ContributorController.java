
package com.cramanager.application.contributor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContributorController {

	@Autowired
	private ContributorService contributorService;

	@RequestMapping(value = "/contributors", method = RequestMethod.GET)
	public List<Contributor> getContributors() {
		return contributorService.getAllContributors();
	}

	@RequestMapping(value = "/contributors/{id}", method = RequestMethod.GET)
	public ResponseEntity<Contributor> getContributor(@PathVariable String id) {
		return new ResponseEntity<>(
				contributorService.findTheContributor(id).get(), HttpStatus.OK);
	}
	// test
	

}
