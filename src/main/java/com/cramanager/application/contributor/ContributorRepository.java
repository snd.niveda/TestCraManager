
package com.cramanager.application.contributor;

import java.util.ArrayList;
import java.util.List;

public class ContributorRepository {

	private List<Contributor> contributors = new ArrayList<>();

	public ContributorRepository() {
		this.contributors.add(
				new Contributor("707", "Dupond", "Sarah"));
		this.contributors.add(
				new Contributor("001", "Durand", "Valentin"));
		this.contributors.add(new Contributor("002", "Alonzo", "Stéphane"));
	}

	public List<Contributor> getAllContributors() {
		// MOCK : Return fixed values
		return contributors;
		
	}

}
