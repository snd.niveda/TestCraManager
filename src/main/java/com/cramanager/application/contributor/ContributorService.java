
package com.cramanager.application.contributor;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

@Component
public class ContributorService {

	private ContributorRepository contributorRepository;

	public ContributorService() {
		contributorRepository = new ContributorRepository();
	}

	public List<Contributor> getAllContributors() {
		return contributorRepository.getAllContributors();
	}

	public Optional<Contributor> findTheContributor(String idContributor) {
		return contributorRepository.getAllContributors().stream()
				.filter(c -> c.getId().equals(idContributor)).findFirst();
	}

}
