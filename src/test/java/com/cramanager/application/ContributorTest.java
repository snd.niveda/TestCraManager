
package com.cramanager.application;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cramanager.application.contributor.Contributor;
import com.cramanager.application.contributor.ContributorService;

@RunWith(SpringRunner.class)
@SpringBootTest

public class ContributorTest {

	private ContributorService contributorService;

	@Test
	public void testService1() {
		contributorService = new ContributorService();
		List<Contributor> contributors = contributorService
				.getAllContributors();
		assertEquals(3, contributors.size());
	}

}
